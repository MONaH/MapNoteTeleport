using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Oxide.Core;
using Oxide.Core.Libraries.Covalence;
using ProtoBuf;
using Rust;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("MapNote Teleport", "MON@H", "2.0.3")]
    [Description("Teleports player to marker on map when placed.")]
    public class MapNoteTeleport : CovalencePlugin
    {
        #region Initialization
        private bool IsGod = false;
        private const string PERMISSION_USE = "mapnoteteleport.use";

        private void Init()
        {
            LoadData();
            permission.RegisterPermission(PERMISSION_USE, this);
            foreach (var command in configData.chatS.commands)
                AddCovalenceCommand(command, nameof(CmdMapNoteTeleport));
        }

        private void OnServerInitialized()
        {
            UpdateConfig();
        }

        private void UpdateConfig()
        {
            if (configData.chatS.commands.Length == 0)
                configData.chatS.commands = new[] { "mnt" };
            SaveConfig();
        }

        private void OnServerSave() => timer.Once(UnityEngine.Random.Range(0f, 60f), SaveData);

        private void Unload()
        {
            SaveData();
        }

        #endregion Initialization

        #region Configuration

        private ConfigData configData;

        private class ConfigData
        {
            [JsonProperty(PropertyName = "Use permissions")]
            public bool usePermission = true;

            [JsonProperty(PropertyName = "Teleportation height (coordinate y)")]
            public short coordinateY = 120;

            [JsonProperty(PropertyName = "Global settings")]
            public GlobalSettings globalS = new GlobalSettings();

            [JsonProperty(PropertyName = "Chat settings")]
            public ChatSettings chatS = new ChatSettings();

            public class GlobalSettings
            {
                [JsonProperty(PropertyName = "Allows admins to teleport without permission")]
                public bool adminsAllowed = true;

                [JsonProperty(PropertyName = "Default enabled")]
                public bool defaultEnabled = true;

                [JsonProperty(PropertyName = "Default cooldown")]
                public float defaultCooldown = 10f;

                [JsonProperty(PropertyName = "Maximum cooldown")]
                public float maximumCooldown = 15f;

                [JsonProperty(PropertyName = "Minimum cooldown")]
                public float minimumCooldown = 5f;
            }

            public class ChatSettings
            {
                [JsonProperty(PropertyName = "Chat command")]
                public string[] commands = new[] { "mnt", "mapnoteteleport" };

                [JsonProperty(PropertyName = "Chat prefix")]
                public string prefix = "<color=#00FFFF>[MapNote Teleport]</color>: ";

                [JsonProperty(PropertyName = "Chat steamID icon")]
                public ulong steamIDIcon = 0;
            }
        }

        protected override void LoadConfig()
        {
            base.LoadConfig();
            try
            {
                configData = Config.ReadObject<ConfigData>();
                if (configData == null)
                    LoadDefaultConfig();
            }
            catch
            {
                PrintError("The configuration file is corrupted");
                LoadDefaultConfig();
            }
            SaveConfig();
        }

        protected override void LoadDefaultConfig()
        {
            PrintWarning("Creating a new configuration file");
            configData = new ConfigData();
        }

        protected override void SaveConfig() => Config.WriteObject(configData);

        #endregion Configuration

        #region DataFile

        private StoredData storedData;

        private class StoredData
        {
            public readonly Dictionary<ulong, PlayerData> playerData = new Dictionary<ulong, PlayerData>();

            public class PlayerData
            {
                public bool enabled;
                public float time;
            }
        }

        private StoredData.PlayerData GetPlayerData(ulong playerID)
        {
            StoredData.PlayerData playerData;
            if (!storedData.playerData.TryGetValue(playerID, out playerData))
            {
                return null;
            }

            return playerData;
        }

        private void LoadData()
        {
            try
            {
                storedData = Interface.Oxide.DataFileSystem.ReadObject<StoredData>(Name);
            }
            catch
            {
                storedData = null;
            }
            finally
            {
                if (storedData == null)
                {
                    ClearData();
                }
            }
        }

        private void SaveData() => Interface.Oxide.DataFileSystem.WriteObject(Name, storedData);

        private void ClearData()
        {
            storedData = new StoredData();
            SaveData();
        }

        private void OnNewSave(string filename)
        {
            SaveData();
        }

        #endregion DataFile

        #region Localization

        private string Lang(string key, string id = null, params object[] args) => string.Format(lang.GetMessage(key, this, id), args);

        protected override void LoadDefaultMessages()
        {
            lang.RegisterMessages(new Dictionary<string, string>
            {
                ["NotAllowed"] = "You do not have permission to use this command",
                ["Enabled"] = "<color=#228B22>Enabled</color>",
                ["Disabled"] = "<color=#B22222>Disabled</color>",
                ["Teleported"] = "Teleported to <color=#FFA500>{0}</color>. Godmode is now <color=#228B22>on</color> for <color=#FFA500>{1}</color>s while teleport is on cooldown.",
                ["CooldownEnded"] = "Godmode is now <color=#B22222>off</color>. You can teleport again.",
                ["MapNoteTeleport"] = "Teleporting to map marker is now {0}",
                ["MapNoteTeleportCooldown"] = "Teleporting to map marker cooldown set to <color=#FFA500>{0}</color>s.",
                ["MapNoteTeleportCooldownLimit"] = "Teleporting to map marker cooldown allowed is between <color=#FFA500>{0}</color>s and <color=#FFA500>{1}</color>s",
                ["MapNoteTeleportMounted"] = "You can't teleport while seated!",
                ["MapNoteTeleportDead"] = "You can't teleport while being dead!",
                ["SyntaxError"] = "Syntax error, type '<color=#FFFF00>/{0} <help | h></color>' to view help",

                ["MapNoteTeleportSyntax"] = "<color=#FFFF00>/{0} </color> - Enable/Disable teleporting to map marker",
                ["MapNoteTeleportSyntax1"] = "<color=#FFFF00>/{0} <time (seconds)></color> - Set teleport cooldown time, the allowed time is between <color=#FFA500>{1}</color>s and <color=#FFA500>{2}</color>s.",
            }, this);
            lang.RegisterMessages(new Dictionary<string, string>
            {
                ["NotAllowed"] = "У вас нет разрешения на использование этой команды",
                ["Enabled"] = "<color=#228B22>Включена</color>",
                ["Disabled"] = "<color=#B22222>Отключена</color>",
                ["Teleported"] = "Вы телепортированы на <color=#FFA500>{0}</color>. Режим Бога <color=#228B22>включен</color> на <color=#FFA500>{1}</color> сек, пока телепорт перезаряжается.",
                ["CooldownEnded"] = "Режим Бога <color=#B22222>отключен</color>. Можете телепортироваться снова.",
                ["MapNoteTeleport"] = "Телепортация к маркеру на карте сейчас {0}",
                ["MapNoteTeleportCooldown"] = "Время перезарядки телепорта к маркеру на карте установлено на <color=#FFA500>{0}</color> секунд.",
                ["MapNoteTeleportCooldownLimit"] = "Значение времени перезарядки телепорта к маркеру на карте должно быть между <color=#FFA500>{0}</color> сек и <color=#FFA500>{1}</color> сек",
                ["MapNoteTeleportMounted"] = "Вы не можете телепортироваться, когда сидите!",
                ["MapNoteTeleportDead"] = "Вы не можете телепортироваться, пока мертвы!",
                ["SyntaxError"] = "Синтаксическая ошибка, напишите '<color=#FFFF00>/{0} <help | h></color>' чтобы отобразить подсказки",

                ["MapNoteTeleportSyntax"] = "<color=#FFFF00>/{0} </color> - Включить/Выключить телепортацию к маркеру на карте",
                ["MapNoteTeleportSyntax1"] = "<color=#FFFF00>/{0} <время (сек.)></color> - Установить время перезарядки телепорта к маркеру на карте. Значение должно быть между <color=#FFA500>{1}</color> сек. и <color=#FFA500>{2}</color> сек.",
            }, this, "ru");
        }

        #endregion Localization

        #region Commands

        private void CmdMapNoteTeleport(IPlayer player, string command, string[] args)
        {

            if (configData.usePermission && !permission.UserHasPermission(player.Id, PERMISSION_USE))
            {
                if (!configData.globalS.adminsAllowed || !player.IsAdmin)
                {
                    Print(player, Lang("NotAllowed", player.Id));
                    return;
                }
            }

            var playerData = GetPlayerData(ulong.Parse(player.Id));
            if (playerData == null)
            {
                playerData = new StoredData.PlayerData
                {
                    enabled = configData.globalS.defaultEnabled,
                    time = configData.globalS.defaultCooldown,
                };
                storedData.playerData.Add(ulong.Parse(player.Id), playerData);
            }

            if (args == null || args.Length == 0)
            {
                playerData.enabled = !playerData.enabled;
                Print(player, Lang("MapNoteTeleport", player.Id, playerData.enabled ? Lang("Enabled", player.Id) : Lang("Disabled", player.Id)));
                return;
            }

            float time;
            if (float.TryParse(args[0], out time))
            {
                if (time <= configData.globalS.maximumCooldown && time >= configData.globalS.minimumCooldown)
                {
                    playerData.time = time;
                    if (!playerData.enabled) playerData.enabled = true;
                    Print(player, Lang("MapNoteTeleportCooldown", player.Id, time));
                    return;
                }
                Print(player, Lang("MapNoteTeleportCooldownLimit", player.Id, configData.globalS.minimumCooldown, configData.globalS.maximumCooldown));
                return;
            }

            switch (args[0].ToLower())
            {
                case "h":
                case "help":
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendLine();
                    var firstCmd = configData.chatS.commands[0];
                    stringBuilder.AppendLine(Lang("MapNoteTeleportSyntax", player.Id, firstCmd));
                    stringBuilder.AppendLine(Lang("MapNoteTeleportSyntax1", player.Id, firstCmd, configData.globalS.minimumCooldown, configData.globalS.maximumCooldown));
                    Print(player, stringBuilder.ToString());
                    return;
            }
            Print(player, Lang("SyntaxError", player.Id, configData.chatS.commands[0]));
        }

        #endregion Commands

        #region Helpers
        private void TP(BasePlayer player, MapNote note)
        {
            var err = CheckPlayer(player);
            if (err != null)
            {
                Print(player.IPlayer, Lang(err, player.UserIDString));
                return;
            }

            IsGod = true;
            player.flyhackPauseTime = 10f;
            var pos = note.worldPosition + new Vector3(0, configData.coordinateY, 0);
            player.Teleport(pos);
            var cooldown = configData.globalS.defaultCooldown;
            var playerData = GetPlayerData(ulong.Parse(player.UserIDString));
            if (playerData != null)
            {
                cooldown = playerData.time;
            }
            Print(player.IPlayer, Lang("Teleported", player.UserIDString, pos, cooldown));

            timer.Once(cooldown, () =>
            {
                IsGod = false;
                if (player == null) return;
                Print(player.IPlayer, Lang("CooldownEnded", player.UserIDString));
            });
        }

        private string CheckPlayer(BasePlayer player)
        {
            if (player.isMounted)
            {
                return "MapNoteTeleportMounted";
            }

            if (!player.IsAlive())
            {
                return "MapNoteTeleportDead";
            }

            return null;
        }

        private void OnMapMarkerAdded(BasePlayer player, MapNote note)
        {
            if (player == null || note == null || IsGod) return;

            if (configData.usePermission && !permission.UserHasPermission(player.UserIDString, PERMISSION_USE))
            {
                if (!configData.globalS.adminsAllowed || !player.IsAdmin)
                {
                    return;
                }
            }

            var playerData = GetPlayerData(player.userID);
            var enabled = configData.globalS.defaultEnabled;
            if (playerData != null) enabled = playerData.enabled;
            if (!enabled) return;

            TP(player, note);
        }

        private void OnEntityTakeDamage(BasePlayer player, HitInfo info)
        {
            if (player == null || !player.userID.IsSteamId()) return;

            if (IsGod) NullifyDamage(ref info);
        }

        private static void NullifyDamage(ref HitInfo info)
        {
            info.damageTypes = new DamageTypeList();
            info.HitMaterial = 0;
            info.PointStart = Vector3.zero;
        }

        private void Print(IPlayer player, string message)
        {
            var text = string.IsNullOrEmpty(configData.chatS.prefix) ? string.Empty : $"{configData.chatS.prefix}{message}";
#if RUST
            (player.Object as BasePlayer).SendConsoleCommand ("chat.add", 2, configData.chatS.steamIDIcon, text);
            return;
#endif
            player.Message(text);
        }

        #endregion Helpers
    }
}